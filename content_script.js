var domReady = function(ready) {
  if (document.readyState != 'loading') return ready();
  document.addEventListener('DOMContentLoaded', ready);
  function _ready() {
    document.removeEventListener('DOMContentLoaded', ready);
    ready();
  }
}

domReady(function() {
  setTimeout(() => {
    var xelem = document.getElementsByClassName('diff-files-holder');
    var i;
    if (xelem[0] == undefined) {
      return false;
    }
    for (i = 0; i < xelem[0].children.length; i++) {
      var e = document.createElement("input");
      var name_id = 'btn_ce_'+ i;
      e.type = 'button';
      e.style.color = "red";
      e.value = 'remove this';
      e.name = name_id;
      e.id = name_id;
      var el = document.getElementById(name_id);
      console.log(el);
      e.onclick = function() {
        this.parentNode.parentNode.removeChild(this.parentNode);
      };
      xelem[0].children[i].prepend(e);
    }
  }, 5000);
});
