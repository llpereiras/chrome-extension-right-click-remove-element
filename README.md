# GitLab remove html element

I created an extension for chrome
to remove html elements from the MR page

A simple chrome extension that let's you remove html element show differ.

No one change will be did in your repository, it's just remove html in your browser.

The extension consists of adding a button
with the function of deleting only the browser html from the changed file

No changes to the git, branch or repository will be made, just in your browser.

### Explanation about chrome extension

https://developer.chrome.com/extensions/getstarted

### Installation

* Open the Extension Management page by navigating to chrome://extensions.
The Extension Management page can also be opened by clicking on the Chrome menu, hovering over More Tools then selecting Extensions.

* Enable Developer Mode by clicking the toggle switch next to Developer mode.

* Click the LOAD UNPACKED button and select the extension directory.



